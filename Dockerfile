FROM node:8-alpine AS runtime
RUN apk add --no-cache curl

WORKDIR /app
ENV NODE_ENV=production
EXPOSE 3000
HEALTHCHECK --interval=5s --timeout=1s CMD curl -sSf "http://localhost:3000/status.json" || exit 1 

COPY package.json yarn.lock /app/
RUN yarn install --production --pure-lockfile

COPY index.js /app/index.js
COPY images /app/images

USER node
CMD ["node", "index.js"]
