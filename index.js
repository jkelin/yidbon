const express = require('express')
const fs = require('fs')
const path = require('path')

const app = express()
const port = process.env.PORT || 3000

const images = fs.readdirSync(path.join(__dirname, 'images')).map(file => path.join(__dirname, 'images', file));

app.get('/status.json', (req, res) => res.json({ status: "ok" }))
app.get('/', (req, res) => res.redirect(307, "https://yidbon.cloud.fireant.pw/img/" + Math.random()))
app.get('*', (req, res) => {
    res.setHeader("Cache-Control", "public, max-age=1");
    res.setHeader("Expires", new Date(Date.now() + 1000).toUTCString());
    res.sendFile(images[Math.floor(Math.random() * images.length)])
})

app.listen(port, () => console.log(`Listening on port ${port}!`))